\documentclass[10pt,a4paper,twoside]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amssymb}
\usepackage[digital,srcmeas,semicon]{circdia}
\usepackage{graphicx}
\graphicspath{ {../design_concept/} } % search path for graphicx

\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

\title{Pad Library design concept for Libresilicon node LS1U}
\author{Ferenc Éger, Hagen Sankowski (co-author)}
\date{2019 November 9}

\makeindex
\setlength{\parindent}{0pt} % get rid of annoying indents

\begin{document}
\maketitle

\begin{abstract}
\begin{quote}

This process is licensed under the Libre Silicon public license; you can redistribute it and/or modify it under the terms of the Libre Silicon public license as published by the Libre Silicon alliance, either version 1 of the License, or (at your option) any later version.

This design is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Libre Silicon Public License for more details.

This document is part of the specification of the free silicon manufacturing standard for manufacturing the LibreSilicon standard logic cells and related free technology nodes from the LibreSilicon project.

\end{quote}
\end{abstract}
\clearpage

\tableofcontents
\clearpage

\pagestyle{headings}

%   ===================================================================

\section{Introduction}

This document is intended to provide an overview of the design concept of the pad cell library for Libresilicon project's LS1U CMOS technology node. This document is a living document: it is expected to change troughout the development process.

%   ===================================================================

\section{Library contents and derivation of cells}

The assortment of cells within the library is selected to satisfy most of the needs encountered in solitary digital based and mixed-signal applications, while keeping the complexity of the design effort reasonably low. The library is intended to be used for automatic generation of the padring of the chip by a higher-level synthesis or Place-and-Route tool, without needing to deal with detailed electrical design and layout considerations at system level. Since pads for applications like high-voltage or RF signals require application-specific and varying design without the possiblity to provide off-the-self solutions, these are excluded from the scope of the library. The following cells are included:

\begin{itemize}
    \item \textbf{CORNER}
    \item \textbf{PADANA}
    \item \textbf{PADFILL}
    \item \textbf{PADGND}
    \item \textbf{PADVDD}
    \item \textbf{PADGIO}
    \item \textbf{PADCIN}
    \item \textbf{PADTIN}
    \item \textbf{PADCOUT}
    \item \textbf{PADTOUT}
    \item \textbf{PADOC}
\end{itemize}

The design effort should be started with designing \textbf{PADGIO}, then \textbf{CORNER}, followed by manual derivation of \textbf{PADVDD} and \textbf{PADGND}, and a tapeout and exhaustive test of the testchip. This may require multiple iterations to have the performance and robustness against ESD optimized. Then, manual derivation of \textbf{PADCIN}, \textbf{PADTIN}, \textbf{PADCOUT}, \textbf{PADTOUT}, \textbf{PADOC}, \textbf{PADANA} and \textbf{PADFILL} may follow. Due to the one-off nature of the design (it shall not be modified by the user unless extensive validation is performed on its behalf), the approach of creating and distributing an algorithm only, that generates the cells automatically, is not recommended.

The description of these pad cells are provided in the following sections.

%   --------    CORNER      -------------------------------------------

\subsection{\textbf{CORNER} Corner cell for padring formation}

This cell does not contain active circuitry. It is intended to form a continuous padring by connecting the ends of the prependicular rows of cells at the outer corners of the chip.

\begin{center}
    \includegraphics[width=\textwidth]{CORNER}
    Simplified layout of \textbf{CORNER} cell
\end{center}

At least four global power supply lines should be supported, \textbf{VDDIO} and \textbf{VSSIO} as well as \textbf{VDDC} and \textbf{VSSC}.
This means, there are two 'dirty' power supply lines dedicted for the io pad frame (big metal), and two 'core' power supply lines nicely filtered and according to the internal voltage level for the core logic.
Both domains, for the io pad supply and the core supply should be de-coupled.

%   --------    PADANA      -------------------------------------------

\subsection{\textbf{PADANA} Pass-trough pad for analog applications}

This cell is intended to provide transparent connection to the pad for analog signals, while providing ESD protection for the circuits behind it. It is derived from \textbf{PADGIO}, by removing input stage, output driver stage, and configuring both ESD transistors as GGNMOS/GGPMOS.

\input{PADANA_schematic.tex}

%   --------    PADFILL     -------------------------------------------

\subsection{\textbf{PADFILL} Dummy (filler) pad}

This cell does not contain active circuitry. It is intended as a placeholder to avoid interruption of the row of pad cells if no pad is desired at the given location, or if the pad is intended to be left unconnected. It is derived from \textbf{PADANA}, by means of removing ESD transistors.

%   --------    PADGND      -------------------------------------------

\subsection{\textbf{PADGND} Ground (negative supply) connection}

This cell is intended to provide ground connection for the chip, including connection to the ground buses of the padring. It is derived from \textbf{PADANA} by removing the n-channel GGNMOS and shorting the pad connection to the ground bus via MET1-MET2 vias.
    Schematic of \textbf{PADANA} cell

\input{PADGND_schematic.tex}

%   --------    PADVDD      -------------------------------------------

\subsection{\textbf{PADVDD} Positive supply connection}

This cell is intended to provide positive supply connection for the chip, including connection to the supply buses of the padring. It is derived from \textbf{PADANA} by removing the p-channel GGPMOS and shorting the pad connection to the supply bus via MET1-MET2 vias.

\input{PADVDD_schematic.tex}

\begin{center}
    \includegraphics[width=\textwidth]{PADsupply_cross}
    Simplified cross-sectional layout of \textbf{PADVDD} and \textbf{PADGND} cells
\end{center}

%   --------    PADGIO      -------------------------------------------

\subsection{\textbf{PADGIO} General-purpose bidirectional digital pad}

This cell is a general-purpose bidirectional digital pad, and also the design basis for most of the other cells. The cell can be subdivided into three parts: the actual bonding pad, the output stage transistors that also double as ESD supressor devices, and the input and control circuitry.

\begin{center}
    \includegraphics[width=\textwidth]{PADIO_cross}
    Simplified cross-sectional layout of \textbf{PADGIO} cell
\end{center}

The bonding pad is implemented as three metal plates on the three metal layers of LS1U. They are connected togerther by an array of vias in a chessboard-like structure. This ensures that the top metal layer is mechanically anchored to the underlying LTO layer, and will not delaminate during bonding and encapsulation.

The output stage transistors are surrounded by a double guardring to prevent latch-up if the pin is overloaded while the chip is biased. During characteristization, this guardring may be removed. The outer guardrings also act as the substrate connection of the input and control circuitry transistors.

\input{PADGIO_circuit.tex}

\textbf{PADGIO} has additional features for more general usages as adaptable driver strength well as switchable pull-up and pull-down. The driver strength is regulated with the \textsc{OE[1:0]} port. Please have a look at the following table.

\begin{center}
    \begin{tabular} { c c | c}
        \multicolumn{2}{c}{ OE[1:0] } & strength \\
        \hline\hline
        0 & 0 & -off- \\
        0 & 1 & 1x \\
        1 & 0 & 2x \\
        1 & 1 & 3x \\
    \end{tabular}
\end{center}

This feature allows applications to tune the driven strength dynamically.

%   --------    PADCIN       -------------------------------------------

\subsection{\textbf{PADCIN} CMOS-level compatible input}

This cell is a digital input with CMOS compatible input voltage levels. It is derived from \textbf{PADGIO} pad cell functionality.

\input{PADCIN_circuit.tex}

\begin{center}
    \begin{tabular} { c | c }
        $V_{IL}$ & $V_{IH}$ \\
        \hline\hline
        0 .. 1/3 VDD & 2/3 VDD .. VDD
    \end{tabular}
\end{center}

Input voltages above $V_{IL}$ and below $V_{IH}$ are ignored by the Schmitt-Trigger hysteresis.

%   --------    PADTIN       -------------------------------------------

\subsection{\textbf{PADTIN} TTL-level compatible input}

This cell is a digital input with TTL compatible input voltage levels. It is derived from \textbf{PADGIO} pad cell functionality.

\input{PADCIN_circuit.tex}

\begin{center}
    \begin{tabular} { c | c }
        $V_{IL}$ & $V_{IH}$ \\
        \hline\hline
        0 .. 0.8V & 2V .. 5V
    \end{tabular}
\end{center}

Input voltages above $V_{IL}$ and below $V_{IH}$ are ignored by the Schmitt-Trigger hysteresis. This cell is feasible with VDDIO = 5 Volt applications only.

%   --------    PADCOUT      -------------------------------------------

\subsection{\textbf{PADCOUT} Digital output}

This cell is a digital output with CMOS-level compatible outputs. It is derived from \textbf{PADGIO} pad cell functionality.

\input{PADTOUT_circuit.tex}

\begin{center}
    \begin{tabular} { c | c }
        $V_{OL}$ & $V_{OH}$ \\
        \hline\hline
        0 .. 1/5 VDD & 4/5 VDD .. VDD
    \end{tabular}
\end{center}

%   --------    PADTOUT      -------------------------------------------

\begin{center}
\subsection{\textbf{PADTOUT} Digital output}

This cell is a digital output with TTL-level compatible outputs. It is derived from \textbf{PADGIO} pad cell functionality.

\input{PADTOUT_circuit.tex}

\begin{center}
    \begin{tabular} { c | c }
        $V_{OL}$ & $V_{OH}$ \\
        \hline\hline
        0 .. 0.4V & 2.4V .. 5V
    \end{tabular}

This cell is feasible with VDDIO = 5 Volt applications only.

%   --------    PADOC        -------------------------------------------

\subsection{\textbf{PADOC} Open-drain current-driving output}

This cell is used for driving loads, eg. Light-emitting diodes (LED) without any additional external line resistor.
Or, to drive CCITT current loops. Hence the internal current-mirror can be adjusted with \textsc{OE[2:0]} to different values regarding the following table.

\input{PADOC_circuit.tex}

This feature allows applications to tune the current dynamically instead of just switching as an open-collector output would do. Functionally it is comparable with a 3-bit Digital-Analog-Converter (DAC) in one pad cell.

\begin{center}
    \begin{tabular} { c c c | c}
        \multicolumn{3}{c}{ OE[2:0] } & current [mA] \\
        \hline\hline
        0 & 0 & 0 & - \\
        0 & 0 & 1 & 3 \\
        0 & 1 & 0 & 6 \\
        0 & 1 & 1 & 9 \\
        1 & 0 & 0 & 12 \\
        1 & 0 & 1 & 15 \\
        1 & 1 & 0 & 18 \\
        1 & 1 & 1 & 21
    \end{tabular}
\end{center}

\end{center}
    \begin{tabular} { c | c }
        $V_{IL}$ & $V_{IH}$ \\
        \hline\hline
        0 .. 1/5 VDD & 4/5 VDD .. VDD
    \end{tabular}
\end{center}

%   ===================================================================

\section{ESD protection concept}

The basic principle of the ESD design is to provide a sufficiently low impedance path between any two of the pins of the IC. When used correctly, a padring formed from this library implements a chip-level ESD protection network similar to the one described in [1] 5.4.2.. Each electrically connected non-power pad has two ESD supressor devices: one GGNMOS to \textsc{GND} and one GGPMOS to \textsc{VDD}. These provide protection in case of pin-pin, pin-\textsc{GND} and pin-\textsc{VDD} discharges. The \textsc{VDD} and \textsc{GND} rings are designed to have sufficiently low impedance. To provide protection against \textsc{VDD}-\textsc{GND} discharges, a power clamp implemented as parallel-connected GGPMOS and GGNMOS devices is distributed along all \textbf{PADVDD} and \textbf{PADGND} cells. The current design goal for the ESD protection is to withstand a load equivalent to a 1KV HBM event. According to experience, this rating, altrough low, is sufficient to survive handling in both industrial mass-production and hobbyist environments with reasonable safety when sufficient ESD protection measures are in place. In later design iterations of the library, this rating may be increased as needed or possible.

\begin{center}
    \includegraphics[width=\textwidth]{ESD_concept}
    Chip-level ESD protection concept
\end{center}

The core part of pin-level ESD protection design is the low-side GGNMOS device that is also used as output transistor where needed. The high-side GGPMOS is derived from it by inverting the doping of the respective regions. The GGNMOS is implemented as a multi-finger structure, with individual fingers having an artificially increased resistance in the drain in order to ensure that the lowest voltage drop yielding to sufficiently high current to cause thermal failure is higher than the highest triggering voltage within the array. This increased impedance is then compensated by connecting multiple fingers in parallel, thus reducing the current trough individual fingers, and consequentially, the overall voltage drop. In the initial design, the resistance is increased by lenghtening the drain diffusion and introducing a portion that is masked by a nitride strip during silicide formation. In the possession of detailed fault analysis results after ESD zapping tests, the dimensioning of these geometries may be optimized in an iterative process. A feature of the GGNMOS structure is that it is formed using the same layers and doping concentrations that are used to form the core logic, ensuring the correlation between the triggering voltage of the GGNMOS and the junction breakdown voltage of core logic transistors.

It is assumed that the n+/pwell and p+/nwell breakdown voltages are lower than the gate oxide breakdown voltage (design target for oxide is 40V), therefore no secondary protection stage is neccessary at CMOS inputs.

%   ===================================================================

\section{Validation and characteristization plan}

In order to validate the pad design from functional and ESD point of view, an 8-pin test chip layout is suggested. This chip is expected to be processed as an ordinary chip (sawn, mounted and bonded to a carrier, either CERDIP, PDIP or PCB). For samples intended for ESD stress test, CERDIP or uncoated PCB is recommended to enable post-test failure analysis. The chip schematic and layout is shown below:

\begin{center}
    \includegraphics[width=\textwidth]{test_chip}
    Test chip for validation
\end{center}

Pins 4 and 8 are the power supply and ground connections, respectively. The \textbf{PADGIO} cell of pin 3 is the DUT (device under test) for most of the tests, with all signals accessible. Pads 5,6 and 7 are used to connect to \textsc{OE}, \textsc{DOUT} and \textsc{DIN} signals of the DUT. Pins 1 and 2 are intended solely for the injection coupling test. It is recommended to carry out the tests on as many samples as possible, and process th results statistically.

%   ===================================================================

\section{Characteristization, functional validation}

The DUT is intended to perform the functional verification of the design (input, output, tristate functions), and parameter characteristization for the most important attributes, like supply voltage range, input levels, output levels under various loads, propagation delays, dynamic power consumption, input capacitance and leakage, output leakage, etc. In order to take into accoutnt the changes made to the library and testing concept during development, the actual test specification shall be written after the first samples are available and the scope of validation is defined.

%   ===================================================================

\section{Injection coupling test}

Injection coupling is a parasitic effect in which, when a current is injected into a pin of a chip with a regular pad layout, the leakage of the physically adjacent pins increase significantly. This can occour, for example, when a digital signal is coupled from an off-chip 5V domain into an on-chip 3V domain using a resistor, relying on the parasitic diode of the pin to clamp the voltage. Another situation of concern is in safety-critical automotive applications, where a controlled functional state must be maintained even in the event of a catastrophic failure, that may result in the 12V supply network being coupled to a low-voltage signal on the PCB. This test is intended to verify the susceptibility of the DUT for this condition. The proposed test setup is shown on the figure below:

\begin{center}
    \includegraphics[width=\textwidth]{injection}
    Scheme of injection coupling test
\end{center}

Pin 3 is set up as an input and it is pulled low via a high resistance (min. 100K). A disturber current of up to 10mA is injected into pin 2, and the level on pin 7 is monitored. The leakage of pin 3 is also monitored. Then, the test is repeated by pulling pin 3 high and injecting negative current into pin 2. It is also recommended to repeat the test with pins 2 and 3 swapping roles. As pass/fail criteria, the level at the DIN signal of the disturbed (non-injected) cell shall not change from the level set by the pullup/pulldown resistor.

%   ===================================================================

\section{ESD characteristization}

In order to assess the effectiveness of the ESD protection network, multiple test chips shall be subjected to ESD zapping tests. The target is to reach at least 1KV ESDV rating for HBM. After the zapping tests, units failing under the targeted rating shall be subjected to extensive failure analysis to determine the root cause of the failures, and optimize the ESD design based on the results. In order to take into accoutnt the availablity of analysis methods and changes made to the library and testing concept during development, the actual test specification shall be written after the first samples are available and the infrastructure to be used for ESD testing and failure analysis is known.

%   ===================================================================

\section{Further improvements}

Currently, the following possible but unadressed feature additions are considered:

\begin{itemize}
    \item Schmitt-triggers at input stages
    \item Switchable weak pullup/pulldown transistors for input and bi-directional pads
    \item Examine the possiblity of locating ESD transistors directly under the bonding pad, thus saving space
    \item Add level shifters and more sophisticated supply and ESD protection schemes to allow for using multiple voltage domains
    \item Integrate sealring into pad and corner layouts
\end{itemize}

%   ===================================================================

\section{References}

[1]: Albert Z. H. Wang: On-chip ESD Protection for Integrated Circuits: An IC Design Perspective

\end{document}
